#!/usr/bin/env python3
from cmdtutil import prnwo as cupw
cupw("here")

# ~ import email
import imaplib

class Mailserver:

    def __init__(self, app):
        self.opt = app.opt
        self.cfg = app.cfg

        
    def closer(self):
        self.server.expunge()
        self.server.close()
        self.server.logout()
        cupw("mailserver expunged, closed and logouted")

    
    def connect(self):
        cupw("connect to mailserver=", self.opt['--mailserver'], self.opt['--mailuser'])
        self.server = imaplib.IMAP4_SSL(self.opt['--mailserver'])
        self.server.login(self.opt['--mailuser'], self.opt['--mailpwd'])
        
        
    def getDraftsNums(self): #>nums (list))
        self.server.select('Drafts')
        typ, data = self.server.search(None, 'ALL')
        # ~ cupw("data[0].split()=", data[0].split())
        # ~ cupw(type(data[0].split()))
        return data[0].split()

            
    def getMailNum(self, num, part):
        typ, data = self.server.fetch(num, '(RFC822)')
        # ~ cupw(typ)
        # ~ cupw(data)
        return typ, data
    

    def delMail(self, num):
        self.server.store(num, '+FLAGS', '\\Deleted')

