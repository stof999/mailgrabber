#!/usr/bin/env python3
from cmdtutil import prnwo as cupw
cupw("here")

import lib.exporter
import os

class Exporter(lib.exporter.Exporter):
    '''
    txt exporter
    - appends news from dict into txt news file
    '''
    

    def __init__(self, app):
        self.opt = app.opt
        self.cfg = app.cfg
        self.exporttype = "txt"
        
    def add(self, filename, maildict): #<filename, maildict
        '''
        formes mail for append in filename
        <filename, maildict
        >null
        '''
        #TODO check if file exist ??? not needed
        #TODO check if NOT append (should not be needed?)
        with open(filename, "a") as fh:
            fh.write(self.formStr(maildict))
            
        
        
        
    def formStr(self, d1):
        str1 = d1['date'] + self.cfg['export']['nl'][self.exporttype]
        str1 += d1['subject'] + self.cfg['export']['nl'][self.exporttype]
        str1 += d1['to'] + self.cfg['export']['nl'][self.exporttype]
        str1 += d1['body'] + self.cfg['export']['nl'][self.exporttype]
        str1 += self.cfg['export']['mailend'][self.exporttype]
        return str1
        
    
    def getExportDir(self):
        return self.opt['--exportdir'] + self.exporttype + "/"    
    
        
    def delAllNewsFiles(self):
        dir1 = self.getExportDir()
        cupw("del all news file=", dir1)
        for f in os.scandir(dir1):
            os.unlink(f.path)
        
