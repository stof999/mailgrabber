#mailgrabber by cmdt.ch
*************************

##what:
i like to use my own (!) mailserver for storing notes, ... as draft emails.
when reading my rss news
i use share (url will be added as first line)
and add text....

mailgrabber catches then the "news email" out from draft folder,
and
- adds them in "news files"
- stores the "news mail" in archive
- and produces news timelines
- "news email" will be deleted if option is set.

all email in draft folder will be processed
when they have something with an "@" in the "to".
for example: "india@news", "it@allg", ...
"india@news" will be append in "news file" india_news.txt
or in an other file according to the mapping list...


##how it works:
- save draft emails with something like "topic@general" as "to"
- run yur (!) mailgrabber script (see example in bin)
- use options (!) (see in code)


##happy news...
cmdt.ch

